import * as React from 'react';
import { View, Text } from 'react-native';
import firestore from '@react-native-firebase/firestore'

export default class App extends React.Component {
  constructor() {
    super()
    this.state = {
      data: {
        nama: 'default name',
        uid: 'default uid'
      },
      listRealTime: [],
      listOneTime: []
    }
  }

  componentDidMount() {
    //RealTime Changes
    firestore()
      .collection('Users')
      .onSnapshot((value) => {
        let tampungan = []
        value.docs.forEach(result => {
          tampungan.push(result.data())
        })
        this.setState({
          listRealTime: tampungan
        })
      })

    //OneTime Changes
    firestore()
      .collection('Users')
      .get()
      .then(value => {
        let tampungan = []
        value.forEach(result => {
          tampungan.push(result.data())
        })
        this.setState({
          listOneTime: tampungan
        })
      })
  }


  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text style={{ fontWeight: 'bold' }}>REAL TIME</Text>
        {this.state.listRealTime.map((value, index) => {
          return (
            <Text key={index}> {value.nama}</Text>
          )
        })}
        <Text style={{ fontWeight: 'bold', marginTop: 10 }}>One TIme</Text>
        {this.state.listOneTime.map((value, index) => {
          return (
            <Text key={index}> {value.nama}</Text>
          )
        })}
      </View>
    );
  }
}
