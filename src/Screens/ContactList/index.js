import React, { Component } from 'react'
import { ScrollView, Text, View, StyleSheet, TextInput, Image, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import Icon2 from 'react-native-vector-icons/FontAwesome5'
import firestore from '@react-native-firebase/firestore'

export class ContactList extends Component {
    constructor() {
        super()
        this.state = {
            data: []
        }
    }

    componentDidMount() {
        firestore()
            .collection('Contact')
            .onSnapshot((value) => {
                let contact = []
                value.docs.forEach(result => {
                    contact.push(result.data())
                })
                this.setState({
                    data: contact
                });
            });
    }

    render() {
        const {data} = this.state
        console.log(data)
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <Text style={{ color: '#0174CF', fontSize: 30, fontWeight: 'bold' }}>Contacts</Text>
                </View>

                <View style={styles.header2}>
                    <View style={styles.search}>
                        <Icon name='search' size={25} style={{ color: '#0174CF', paddingLeft: 20 }} />
                        <TextInput style={{ color: '#0174CF', flex: 1, paddingHorizontal: 20, fontSize: 20 }} placeholder='Search' />
                    </View>
                    <TouchableOpacity style={{ width: 50, height: 50, borderRadius: 10,borderWidth:0.5 , borderColor:'#0174CF', backgroundColor: 'white', alignItems: 'center', justifyContent: 'center' }}>
                        <Icon name='plus' size={25} style={{ color: '#0174CF' }} />
                    </TouchableOpacity>
                </View>

                <Text style={{ fontSize: 25, color: '#0174CF', fontWeight: 'bold', marginLeft: 30, marginBottom: 10 }}>My Contact</Text>

                <ScrollView>
                    {this.state.data.map((value, index) => (
                        <View key={index} style={styles.box}>
                            <Image  source={{ uri: value.foto }} style={styles.poster} />
                            <View style={{ flex: 1, justifyContent: 'space-around', paddingLeft: 20 }}>
                                <Text style={{ color: '#0174CF', fontSize: 20, fontWeight: 'bold' }}>{value.nama}</Text>
                                <Text style={{ color: '#5C5B71', fontSize: 15 }}>Mobile Phone </Text>
                            </View>
                        </View>
                    ))}
                </ScrollView>

                <View style={styles.footer}>
                    <TouchableOpacity>
                        <Icon name='star' size={25} style={{ color: '#0174CF' }} />
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Icon2 name='clock' size={25} style={{ color: '#0174CF' }} />
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Icon name='th' size={25} style={{ color: '#0174CF' }} />
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Icon name='phone' size={25} style={{ color: '#0174CF' }} />
                    </TouchableOpacity>
                </View>

            </View>
        )
    }
}

export default ContactList

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    header: {
        width: '100%',
        height: 70,
        padding: 15
    },
    header2: {
        width: '100%',
        height: 70,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 15,
    },
    search: {
        width: 320,
        height: 50,
        borderRadius: 10,
        borderWidth: 0.5,
        borderColor: '#0174CF',
        backgroundColor: 'white',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    footer: {
        width: '100%',
        height: 60,
        backgroundColor: 'white',
        borderWidth: 0.5,
        borderColor:'#0174CF',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        marginTop: 5,
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30
    },
    box: {
        backgroundColor: 'white',
        width: '92%',
        height: 80,
        borderRadius: 10,
        borderWidth: 0.5,
        borderColor:'#0174CF',
        padding: 10,
        flexDirection: 'row',
        marginLeft: 15,
        marginVertical: 8
    },
    poster: {
        width: '20%',
        height: '100%',
        borderRadius: 10,
        borderWidth: 0.5,
    }
})
