import React, { Component } from 'react'
import { Text, View, StyleSheet, TextInput, TouchableOpacity } from 'react-native'
import firestore from '@react-native-firebase/firestore';

class CRUDFirestore extends Component {
    constructor() {
        super()
        this.state = {
            inputNama: '',
            inputAlamat: ''
        }
    }
    submit = () => {
        firestore()
            .collection('Users')
            .add({
                nama: this.state.inputNama,
                alamat: this.state.inputAlamat,
            })
            .then(() => {
                this.setState({
                    inputNama: '',
                    inputAlamat: ''
                })
            });
    }
    render() {
        return (
            <View style={styles.container}>
                <TextInput style={styles.input} placeholder='Nama' onChangeText={(e) => this.setState({ inputNama: e })} value={this.state.inputNama} />
                <TextInput style={styles.input} placeholder='Alamat' onChangeText={(e) => this.setState({ inputAlamat: e })} value={this.state.inputAlamat} />
                <TouchableOpacity style={styles.button} onPress={this.submit}>
                    <Text style={{ color: '#fff' }}>SUBMIT</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

export default CRUDFirestore

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#79CBE5'
    },
    input: {
        width: 270,
        paddingHorizontal: 15,
        borderWidth: 0.3,
        borderRadius: 50,
        marginTop: 20,
        marginHorizontal: 20,
        elevation: 5,
        backgroundColor: 'white',
        borderColor: '#0174CF',
        alignItems: 'center',
    },
    button: {
        width: 180,
        paddingVertical: 16,
        marginTop: 20,
        borderRadius: 50,
        alignItems: 'center',
        borderWidth: 1,
        borderColor: '#0174CF',
        elevation: 5,
        backgroundColor: '#0174CF'
    }
})

