import React, { Component } from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity, ScrollView } from 'react-native'
import CButton from '../../Component/CButton';

class Dashboard extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Text style={{ color: '#0174CF', fontSize: 20, fontWeight:'bold', marginTop: 20 }}> Select Challenge </Text>

                <TouchableOpacity style={styles.button}>
                    <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: 18 }} onPress={() => this.props.navigation.navigate('ContactList')}> Contact List </Text>
                </TouchableOpacity>

                <TouchableOpacity style={styles.button}>
                    <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: 18 }} onPress={() => this.props.navigation.navigate('CRUDAsync')}> CRUD Async </Text>
                </TouchableOpacity>

                <TouchableOpacity style={styles.button}>
                    <Text style={{ color: '#fff', fontWeight: 'bold', fontSize: 18 }} onPress={() => this.props.navigation.navigate('Inbox')}> Inbox </Text>
                </TouchableOpacity>

                <CButton title='Log Out'/>

                <Text style={{ color: '#0174CF', fontSize: 10, marginTop: 100 }}> Design by zullamuhammad</Text>
            </View>

        )
    }
}

export default Dashboard

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        padding: 20
    },
    button: {
        width: 180,
        height: 100,
        paddingVertical: 16,
        marginTop: 20,
        borderRadius: 10,
        alignItems: 'center',
        borderWidth: 1,
        borderColor: '#0174CF',
        elevation: 5,
        backgroundColor: '#0174CF',
        justifyContent: 'center',
        alignItems: 'center'
    }
}

)