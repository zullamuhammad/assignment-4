import React, { Component } from 'react'
import { Text, View, StyleSheet, Button, TextInput, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/AntDesign'
import AsyncStorage from '@react-native-async-storage/async-storage'

export class CRUDAsync extends Component {
    constructor() {
        super()
        this.state = {
            nama: '',
            alamat: '',
            mainData: []
        }
    }
    async componentDidMount(){
        await this.getData()
    }

    getData = async () => {
        try{
            const jsonValue = await AsyncStorage.getItem('data')
            this.setState({
                mainData: jsonValue != null ? JSON.parse(jsonValue) : []
            })
        } catch(e) {
            //error reading value
        }
    }

    storeData = async () => {
        const {nama, alamat} = this.state
        const json = [...this.state.mainData,{nama: nama, alamat: alamat}]
        try {
            const jsonValue = JSON.stringify(json)
            await AsyncStorage.setItem('data', jsonValue)
            await this.getData()
        }catch (e) {
            //saving error
        }
    }

    async removeItemValue(key){
        try {
            await AsyncStorage.removeItem('data', key);
            return true;
        }
        catch(exception){
            return false;
        }

    }

    render() {
        return (
            <View style={styles.container}>
                <View style={{ width: '80%', height: 300, backgroundColor: '#40514E', elevation: 10, }}>
                    <Text style={{ fontSize: 20, color: 'white', alignSelf: 'center', padding: 10 }}> Add Data </Text>
                    <TextInput onChangeText={(text) => this.setState({ nama: text })} placeholder='Nama' style={styles.input} value={this.state.nama} />
                    <TextInput onChangeText={(text) => this.setState({ alamat: text })} placeholder='Alamat' style={styles.input} value={this.state.alamat} />
                    <Button title='Add' onPress={() => this.storeData()} />
                </View>
                <View style={{ flexDirection: 'row', borderWidth: 0.5, width: '90%', height: 30, backgroundColor: '#30E3CA', marginTop: 20 }}>
                    <Text style={styles.Text}> Nama </Text>
                    <Text style={styles.Text}> Alamat </Text>
                    <Text style={styles.Text}> Aksi </Text>
                </View>
                {this.state.mainData.map((value, index) => (
                    <View key={index} style={{ flexDirection: 'row', borderWidth: 0.5, width: '90%', height: 30, backgroundColor: '#11999E' }}>
                        <Text style={styles.Text}>{value.nama}</Text>
                        <Text style={styles.Text}>{value.alamat}</Text>
                        <TouchableOpacity onPress={() => this.removeItemValue(value.nama)}>
                            <Icon name='delete' size={20} style={{ color: 'black' }} />
                        </TouchableOpacity>
                    </View>
                ))}
            </View>
        )

    }

}

export default CRUDAsync

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#E4F9F5',
        justifyContent: 'center',
        alignItems: 'center',
    },
    Text: {
        flex: 1,
        marginHorizontal: 5,
        fontWeight: 'bold',
    },
    input: {
        width: 270,
        paddingHorizontal: 15,
        borderWidth: 0.3,
        marginTop: 10,
        marginHorizontal: 10,
        elevation: 5,
        backgroundColor: 'white',
        borderColor: '#0174CF',
        alignItems: 'center',
    }
})