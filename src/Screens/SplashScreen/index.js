import React, { Component } from 'react';
import {View, Image} from 'react-native';
import Logo from '../../Assets/logochat.png';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default class SplashScreen extends Component {
   async componentDidMount(){
       const data = await this.getData();
       console.log(data);
       setTimeout(() => {
           if (data != null){
               this.props.navigation.replace('Dashboard');
           }else {
               this.props.navigation.replace('Login');
           }
       }, 3000);
   }

   getData = async () => {
       try {
           const valueData = await AsyncStorage.getItem('dataNew');
           return valueData != null ? JSON.parse(valueData): null;
       }catch (error){}
   };

    render() {
        return (

            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor:'#fff' }}>
                <Image source={Logo} style={{width: 280, height: 210}} />
            </View>

        )
    }
}
