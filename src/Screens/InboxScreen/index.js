import AsyncStorage from '@react-native-async-storage/async-storage'
import React, { Component } from 'react'
import { ScrollView, Text, View, StyleSheet, TextInput, Image, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/Feather'

export class Inbox extends Component {
    constructor(props) {
        super(props);
        this.state = {
            inbox: [{
                title: '',
                body: '',
            }
            ]
        }
    }
    componentDidMount() {
        AsyncStorage.getItem('inbox').then((resultInbox) => {
            const inbox = JSON.parse(resultInbox)
            console.log(resultInbox)
            if (resultInbox) {
                this.setState({
                    inbox: inbox
                })
            }
        }).catch((err) => {

        })
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <Text style={{ color: '#0174CF', fontSize: 30, fontWeight: 'bold' }}> Inbox</Text>
                    <Image  source={{ uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQFvJlq4whQUnI3c3jzSQ_CYW4IkZFHfj-K1g&usqp=CAU'}} style={styles.photo} />
                </View>

                <View style={styles.header2}>
                    <View style={styles.search}>
                        <TextInput style={{ color: 'white', flex: 1, paddingHorizontal: 20, fontSize: 15 }} placeholder='Search for message....'/>
                        <TouchableOpacity>
                            <Icon name='search' size={25} style={{ color: '#0174CF', paddingRight: 20 }} />
                        </TouchableOpacity>
                    </View>
                </View>

                <ScrollView>
                    {this.state.inbox.map((value, index) => (
                        <View key={index} style={styles.box}>
                            <View style={styles.photo}>
                            <Icon name='user' size={35} style={{ color: 'white'}} />
                            </View> 
        
                            <View style={{ flex: 1, justifyContent: 'space-around', paddingLeft: 20 }}>
                                <Text style={{ color: '#0174CF', fontSize: 15, fontWeight: 'bold' }}>{value.title}</Text>
                                <Text style={{ color: '#5C5B71', fontSize: 15 }}>{value.body}</Text>
                            </View>
                            <View style={{alignSelf:'flex-start'}}>
                            <Text style={{ color: '#5C5B71', fontSize: 10, }}>10.00 AM</Text>
                            </View>
                        </View>
                    ))}
                </ScrollView>

                <View style={styles.footer}>
                    <TouchableOpacity>
                        <Icon name='message-square' size={25} style={{ color: '#0174CF' }} />
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Icon name='phone' size={25} style={{ color: '#0174CF' }} />
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Icon name='camera' size={25} style={{ color: '#0174CF' }} />
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Icon name='settings' size={25} style={{ color: '#0174CF' }} />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

export default Inbox

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    header: {
        width: '100%',
        height: 100,
        padding: 15,
        flexDirection: 'row',
        justifyContent:'space-between',
        alignItems:'center'
    },
    header2: {
        width: '100%',
        height: 70,
        flexDirection: 'row',
        alignItems:'center',
        paddingHorizontal: 15,
    },
    search: {
        width: '100%',
        height: 50,
        borderRadius: 30,
        borderColor:'#0174CF',
        borderWidth: 0.5,
        backgroundColor: 'white',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    footer: {
        width: '100%',
        height: 60,
        backgroundColor: 'white',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        marginTop: 5,
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        borderWidth: 0.3,
        borderColor:'#0174CF'
    },
    box: {
        backgroundColor: 'white',
        flex: 1,
        height: 80,
        alignItems:'center',
        flexDirection: 'row',
        marginVertical: 5,
        padding: 10
    },
    photo: {
        width: 70,
        height: 70,
        borderRadius: 50,
        backgroundColor: '#0174CF',
        justifyContent:'center',
        alignItems:'center'
    }
})
