import React, { Component } from 'react'
import { Text, TouchableOpacity, StyleSheet } from 'react-native'

export class CButton extends Component {
    render() {
        return (
            <TouchableOpacity style={styles.button}>
                <Text style={{ color: '#fff' }}>{this.props.title}</Text>
            </TouchableOpacity>
        )
    }
}

export default CButton

const styles = StyleSheet.create ({
    button: {
        width: 180,
        paddingVertical: 16,
        marginTop: 20,
        borderRadius: 50,
        alignItems: 'center',
        borderWidth: 1,
        borderColor: '#0174CF',
        elevation: 5,
        backgroundColor: '#0174CF'
      }
})
