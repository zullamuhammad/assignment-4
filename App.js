import 'react-native-gesture-handler';
import React from 'react';
import type { Node } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import SplashScreen from './src/Screens/SplashScreen';
import LoginForm from './src/Screens/LoginScreen';
import SignUp from './src/Screens/SignUpScreen';
import Dashboard from './src/Screens/DashboardScreen';
import ContactList from './src/Screens/ContactList';
import CRUDAsync from './src/Screens/CRUDAsync';
import messaging from '@react-native-firebase/messaging';
import Inbox from './src/Screens/InboxScreen';
import { Alert } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Stack = createStackNavigator();
const hide = { headerShown: false }

const App: () => Node = () => {
  messaging().onMessage(async remoteMessage => {
    Alert.alert('Anda menerima pesan baru : ', JSON.stringify(remoteMessage.notification.body.title));
    const title = remoteMessage.notification.title;
    const body = remoteMessage.notification.body;
    AsyncStorage.getItem('inbox')
      .then(resultInbox => {
        const inbox = JSON.parse(resultInbox)
        AsyncStorage.setItem('inbox', JSON.stringify([{ title: title, body: body}
          , ...inbox]
        ))
      }).catch(err => {
        AsyncStorage.setItem('inbox',
          JSON.stringify([{ title: title, body: body}]))
      })
  });

  messaging()
    .getToken()
    .then((token) => console.log(token))

  // Register background handler
  messaging().setBackgroundMessageHandler(async remoteMessage => {
    const title = remoteMessage.notification.title;
    const body = remoteMessage.notification.body;
    AsyncStorage.getItem('inbox')
      .then(resultInbox => {
        const inbox = JSON.parse(resultInbox)
        AsyncStorage.setItem('inbox', JSON.stringify([{ title: title, body: body}
          , ...inbox]
        ))
      }).catch(err => {
        AsyncStorage.setItem('inbox', JSON.stringify([{ title: title, body: body}]))
      })
    console.log('Message handled in the background!', remoteMessage);
  });

  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Dashboard">
        <Stack.Screen name="Loading" component={SplashScreen} options={hide} />
        <Stack.Screen name="Login" component={LoginForm} options={hide} />
        <Stack.Screen name="SignUp" component={SignUp} options={hide} />
        <Stack.Screen name="Dashboard" component={Dashboard} options={hide} />
        <Stack.Screen name="ContactList" component={ContactList} options={hide} />
        <Stack.Screen name="CRUDAsync" component={CRUDAsync} options={hide} />
        <Stack.Screen name="Inbox" component={Inbox} options={hide} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
